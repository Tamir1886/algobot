from ibapi.contract import Contract
from Classes.IBApi import IBapi
import threading
import time
import os
from dotenv import load_dotenv

load_dotenv()

app = IBapi()
app.connect(os.getenv("HOST_ADDRESS"), int(os.getenv("PORT")), int(os.getenv("CLIENT_ID")))

api_thread = threading.Thread(target=app.run, daemon=True)
api_thread.start()

time.sleep(1)

contract = Contract()
# contract.symbol = os.getenv("SYMBOL")
# contract.secType = "OPT"
# contract.exchange = "SMART"
# contract.currency = os.getenv("CURRENCY")
# contract.lastTradeDateOrContractMonth = "20210511"
# contract.strike = int(os.getenv("STRIKE"))
# contract.right = "C"

contract.symbol = "FISV"

contract.secType = "OPT"

contract.exchange = "SMART"

contract.currency = "USD"
app.reqContractDetails(210, contract)


while app.isConnected():
#     # app.reqSecDefOptParams(0, contract.symbol, "", "STK", 8314)
    time.sleep(int(os.getenv("SAMPLE_INTERVAL_SECONDS")))
    continue

app.disconnect()













