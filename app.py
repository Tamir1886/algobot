from ibapi.contract import Contract
from Classes.IBApi import IBapi
import threading
import time

app = IBapi()
app.connect('127.0.0.1', 7496, 123)

# Start the socket in a thread
api_thread = threading.Thread(target=app.run, daemon=True)
api_thread.start()

time.sleep(1)  # Sleep interval to allow time for connection to server

# Create contract object
apple_contract = Contract()
apple_contract.symbol = 'APPL'
apple_contract.secType = 'OPT'
apple_contract.exchange = 'SMART'
apple_contract.currency = 'USD'

# Request Market Data
app.reqMktData(1, apple_contract, '', False, False, [])

time.sleep(10)  # Sleep interval to allow time for incoming price data
app.disconnect()
