from ibapi.client import EClient, SetOfString, SetOfFloat, TickerId
from ibapi.wrapper import EWrapper, TickType, ContractDetails


class IBapi(EWrapper, EClient):
    def __init__(self):
        EClient.__init__(self, self)

    def tickPrice(self, request_id, tick_type, price, attribute):
        print("got request")
        print(tick_type)
        if tick_type == 2 and request_id == 1:
            print('The current ask price is: ', price)
        if tick_type == 11:
            print(tick_type)

    def securityDefinitionOptionParameter(self, reqId: int, exchange: str,
                                          underlyingConId: int, tradingClass: str, multiplier: str,
                                          expirations: SetOfString, strikes: SetOfFloat):
        super().securityDefinitionOptionParameter(reqId, exchange,
                                                  underlyingConId, tradingClass, multiplier, expirations, strikes)

        # with open('results.txt', 'a+') as res_file:
        results = f"SecurityDefinitionOptionParameter. ReqId: {reqId} \nExchange: {exchange} \nUnderlying conId: {underlyingConId} \nTradingClass: {tradingClass} \nMultiplier: {multiplier} \nExpirations: {expirations} \nStrikes: {strikes} \n"
        print(results)

    def tickOptionComputation(self, reqId: TickerId, tickType: TickType, tickAttrib: int,
                              impliedVol: float, delta: float, optPrice: float,
                              pvDividend: float,
                              gamma: float, vega: float, theta: float, undPrice: float):
        super().tickOptionComputation(reqId, tickType, tickAttrib, impliedVol, delta,

                                      optPrice, pvDividend, gamma, vega, theta, undPrice)

        print("Delta:", delta)

    def contractDetails(self, reqId: int, contractDetails: ContractDetails):
        super().contractDetails(reqId, contractDetails)

        # print(contractDetails)
        print("strike:", contractDetails.contract.strike)
        print("last Trade Date Or Contract Month:", contractDetails.contract.lastTradeDateOrContractMonth)
        self.reqMktData(1013, contractDetails.contract, "", False, False, [])
        self.calculateOptionPrice(5002, contractDetails.contract, 0.6, 55, [])

    def contractDetailsEnd(self, reqId: int):

        super().contractDetailsEnd(reqId)

        print("ContractDetailsEnd. ReqId:", reqId)

    def tickSnapshotEnd(self, reqId: int):

        super().tickSnapshotEnd(reqId)

        print("TickSnapshotEnd. TickerId:", reqId)
